window.addEventListener('DOMContentLoaded', async () => {
    let response = await fetch ('http://localhost:8000/api/locations/')

    if (!response.ok) {
        //throw an error
    } else {
        const data = await response.json();
        const selectTag = document.querySelector('#location')

        for (let location of data.locations) {
            let option = document.createElement('option')
            option.value = location.id
            option.innerHTML = location.name
            selectTag.appendChild(option);
        }
        const formTag = document.getElementById('create-conference-form');
        formTag.addEventListener('submit', async event => {
            event.preventDefault();
            const formData = new FormData(formTag);
            const json = JSON.stringify(Object.fromEntries(formData));
            const conferenceUrl = 'http://localhost:8000/api/conferences/';
            const fetchConfig = {
                method: 'POST',
                body: json,
                headers: {
                    'Content-Type': 'application/json',
                },
            };
            response = await fetch(conferenceUrl, fetchConfig);
            if (response.ok) {
                formTag.reset();
                const newConference = await response.json();
                console.log(newConference);
            }
        })
    }
})
