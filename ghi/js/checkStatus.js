window.addEventListener('DOMContentLoaded', async () => {
    const payloadCookie = await cookieStore.get('jwt_access_payload')
    if (payloadCookie) {
        // The cookie value is a JSON-formatted string, so parse it
        const encodedPayload = JSON.parse(payloadCookie.value);

        // Convert the encoded payload from base64 to normal string
        const decodedPayload = atob(encodedPayload)

        // The payload is a JSON-formatted string, so parse it
        const payload = JSON.parse(decodedPayload)

        // Print the payload
        console.log(payload.user.perms[24]);

        if (payload.user.perms.includes('events.add_conference')) {
            const newConf = document.getElementById('new-conference')
            newConf.classList.remove('d-none');
        }
        if (payload.user.perms.includes('events.add_location')) {
            const newLoc = document.getElementById('new-location')
            newLoc.classList.remove('d-none');
        }
        if (payload.user.perms.includes('presentations.add_presentation')) {
            const newPres = document.getElementById('new-presentation')
            newPres.classList.remove('d-none');
        }
    };
})
