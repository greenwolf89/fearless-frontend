window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
            alert('There was a problem loading this page', 'danger')
        } else {
            const data = await response.json();
            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                const html = cardPlaceholder();
                const container = document.querySelector('.container');
                container.innerHTML += html;
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const name = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const location = details.conference.location.name;
                    const starts = (new Date(details.conference.starts)).toLocaleDateString();
                    const ends = (new Date(details.conference.ends)).toLocaleDateString();
                    const html = createCard(name, starts, ends, location, description, pictureUrl);
                    const container = document.querySelector('.container');
                    container.innerHTML += html;
                    const placeholders = document.querySelectorAll('.placeholder-wave')
                    for (let ph of placeholders) {
                        ph.setAttribute('style', 'display: none');
                    }
                }
            }

        }
    } catch (e) {
        alert('Something went wrong!', 'light')
    }
});

const alertPlaceholder = document.getElementById('liveAlertPlaceholder')

function alert(message, type) {
    const wrapper = document.createElement('div');
    wrapper.innerHTML = `
        <div class="alert alert-${type} alert-dismissible" role="alert">${message}
        </div>`

    alertPlaceholder.append(wrapper);
}

function cardPlaceholder() {
    return `
      <div class="card placeholder-wave">
        <img src="..." class="card-img-top" alt="">
        <div class="card-body">
          <h5 class="card-title placeholder-glow bg-secondary"></h5>
          <h6 class="card-subtitle mb-2 text-muted placeholder-glow bg-info"></h6>
          <p class="card-text placeholder-glow bg-light"></p>
        </div>
        <div class="card-footer placeholder-glow">
        </div>
      </div>
    `;
}

function createCard(name, starts, ends, location, description, pictureUrl) {
    return `
      <div class="card">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
            ${starts} - ${ends}
        </div>
      </div>
    `;
}
