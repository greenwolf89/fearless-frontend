window.addEventListener('DOMContentLoaded', async () => {
    try {
        let response = await fetch('http://localhost:8000/api/states/');

        if (!response.ok) {
            //throw an error
        } else {
            const data = await response.json();
            const selectTag = document.querySelector('#state')

            for (let state of data.states) {
                let option = document.createElement('option')
                option.value = state.abbreviation
                option.innerHTML = state.name
                selectTag.appendChild(option);
            }
            const formTag = document.getElementById('create-location-form');
            formTag.addEventListener('submit', async event => {
                event.preventDefault();
                const formData = new FormData(formTag);
                const json = JSON.stringify(Object.fromEntries(formData));
                const locationUrl = 'http://localhost:8000/api/locations/';
                const fetchConfig = {
                    method: 'POST',
                    body: json,
                    headers: {
                        'Content-Type': 'application/json',
                    },
                };
                response = await fetch(locationUrl, fetchConfig);
                if (response.ok) {
                    formTag.reset();
                }
            })
        }
    } catch (e) {
        //throw an error
    }
});
