window.addEventListener('DOMContentLoaded', async () => {
    let response = await fetch ('http://localhost:8000/api/conferences/')

    if (!response.ok) {
        //throw an error
    } else {
        const data = await response.json();
        const selectTag = document.querySelector('#conference')

        for (let conference of data.conferences) {
            let option = document.createElement('option')
            option.value = conference.id
            option.innerHTML = conference.name
            selectTag.appendChild(option);
        }
        var conf = 0;
        selectTag.addEventListener('change', async event => {
            event.preventDefault();
            conf = event.target.value;
        });
        const formTag = document.getElementById('create-presentation-form');
        formTag.addEventListener('submit', async event => {
            event.preventDefault();
            const formData = new FormData(formTag);
            const json = JSON.stringify(Object.fromEntries(formData));
            const presentationUrl = `http://localhost:8000/api/conferences/${conf}/presentations/`;
            const fetchConfig = {
                method: 'POST',
                body: json,
                headers: {
                    'Content-Type': 'application/json',
                },
            };
            response = await fetch(presentationUrl, fetchConfig);
            if (response.ok) {
                formTag.reset();
                const newPresentation = await response.json();
                console.log(newPresentation);
            }
        })
    }
})
