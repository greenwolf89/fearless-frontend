import React from 'react';
import { NavLink } from "react-router-dom"

function Nav() {
    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <div className="container-fluid">
        <a className="navbar-brand" href="http://localhost:3000/#">Conference GO!</a>
        </div>
        <div className="container-fluid">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <a className="nav-link active" aria-current="page" href="http://localhost:3000/#">Home</a>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" id="new-location" aria-current="page" to="locations/new">New location</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" id="new-conference" aria-current="page" to="conferences/new">New conference</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" id="new-presentation" aria-current="page" to="presentations/new">New presentation</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" id="new-attendee" aria-current="page" to="attendees/new">Attend Conference</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" id="show-attendees" aria-current="page" to="attendees">Attend Conference</NavLink>
            </li>
          </ul>
          </div>
        </nav>
    )
  }

  export default Nav;
