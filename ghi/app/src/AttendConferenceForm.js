import React from 'react';

class AttendConferenceForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            conferences: [],
            name: "",
            email: "",
            companyName: "",
            conference: "",
            submitted: false,
        };
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleEmailChange = this.handleEmailChange.bind(this);
        this.handleCompanyNameChange = this.handleCompanyNameChange.bind(this);
        this.handleConferenceChange = this.handleConferenceChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        data.company_name = data.companyName;
        delete data.companyName;
        delete data.conferences;
        delete data.submitted; //we need submitted to be in the state but not in the post request body

        const attendeeUrl = 'http://localhost:8001/api/attendees/';
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(attendeeUrl, fetchConfig);
        if (response.ok) {
            const cleared = {
                name: "",
                email: "",
                companyName: "",
                conference: "",
                submitted: true,
            };
            this.setState(cleared);
        }
    }

    handleNameChange(event) {
        const value = event.target.value;
        this.setState({ name: value })
    }
    handleEmailChange(event) {
        const value = event.target.value;
        this.setState({ email: value })
    }
    handleCompanyNameChange(event) {
        const value = event.target.value;
        this.setState({ companyName: value })
    }
    handleConferenceChange(event) {
        const value = event.target.value;
        this.setState({ conference: value })
    }

    async componentDidMount() {
        const response = await fetch('http://localhost:8000/api/conferences')

        if (response.ok) {
            const data = await response.json();
            this.setState({ conferences: data.conferences });
        }
    }

    render() {
        let spinnerClasses = "d-flex justify-content-center mb-3"
        let dropdownClasses = "form-select d-none"
        if(this.state.conferences.length > 0){
            spinnerClasses = "d-none"
            dropdownClasses = "form-select"
        }
        let formClass = "attendee-form"
        let successClass = "alert alert-success d-none mb-0"
        if(this.state.submitted === true){
            formClass = "d-none"
            successClass = "alert alert-success mb-0"
        }
        return (
            <div className="my-5" container>
                <div className="row">
                    <div className="col col-sm-auto">
                        <img width="300" className="bg-white rounded shadow d-block mx-auto mb-4" src="/logo.svg" alt=""/>
                    </div>
                    <div className="col">
                        <div className="card shadow">
                            <div className="card-body">
                                <form className={formClass} onSubmit={this.handleSubmit} id="create-attendee-form">
                                    <h1 className="card-title">It's Conference Time!</h1>
                                    <p className="mb-3">
                                        Please choose which conference
                                        you'd like to attend.
                                    </p>
                                    <div className={spinnerClasses} id="loading-conference-spinner">
                                        <div className="spinner-grow text-secondary" role="status">
                                            <span className="visually-hidden">Loading...</span>
                                        </div>
                                    </div>
                                    <div className="mb-3">
                                        <select value={this.state.conference} onChange={this.handleConferenceChange} name="conference" id="conference" className={dropdownClasses} required>
                                            <option>Choose a conference</option>
                                            {this.state.conferences.map(conference => {
                                                return (
                                                    <option key={conference.href} value={conference.href}>
                                                        {conference.name}
                                                    </option>
                                                );
                                            })}
                                        </select>
                                    </div>
                                    <p className="mb-3">
                                        Now, tell us about yourself.
                                    </p>
                                    <div className="row">
                                        <div className="col">
                                            <div className="form-floating mb-3">
                                                <input value={this.state.name} onChange={this.handleNameChange} required placeholder="Your full name" type="text" id="name" name="name" className="form-control" />
                                                <label htmlFor="name">Your full name</label>
                                            </div>
                                        </div>
                                        <div className="col">
                                            <div className="form-floating mb-3">
                                                <input value={this.state.email} onChange={this.handleEmailChange} required placeholder="Your email address" type="email" id="email" name="email" className="form-control" />
                                                <label htmlFor="email">Your email address</label>
                                            </div>
                                        </div>
                                        <div className="col">
                                            <div className="form-floating mb-3">
                                                <input value={this.state.companyName} onChange={this.handleCompanyNameChange} required placeholder="Your email address" type="text" id="company_name" name="company_name" className="form-control" />
                                                <label htmlFor="email">Your company name</label>
                                            </div>
                                        </div>
                                    </div>
                                    <button className="btn btn-lg btn-primary">I'm going!</button>
                                </form>
                                <div className={successClass} id="success-message">
                                    Congratulations! You're all signed up!
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default AttendConferenceForm;
